#pragma once

#include <vector>

#include "argType.h"

class ArgParser{
public:
    ArgParser();
    ArgParser(int argc, char* argv[]);
    std::vector<ArgType> argsTypes() const;
private:
    int _argc;
    char** _argv;
};