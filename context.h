#include <vector>
#include <string>

#include "argType.h"
#include "argParser.h"

class Context{
    
public:
    Context();
    Context(const ArgParser& argParser);
    Context(std::vector<ArgType> argsTypes);
    Context(int argsCount, std::vector<ArgType> argsTypes);

    void setArgParser(const ArgParser& argParser);
    std::vector<std::string> generate();
private:
    int randomIntGenerator(int upperBound = INT32_MAX);
    std::string randomStrGenerator();
    
private:
    int _argsCount;
    std::vector<ArgType> _argsTypes; 
};