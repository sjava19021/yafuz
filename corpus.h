#pragma once

#include <queue>
#include <string>

class Corpus{
public:
    Corpus();

private:
    std::queue<std::string> q;
};