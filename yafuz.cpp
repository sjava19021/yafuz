#include <iostream>
#include <fstream>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <regex>

#include "context.h"
#include "corpus.h"
#include "argParser.h"

using namespace std;

float getPercentage(std::string s){
    std::smatch m;
    std::regex e ("\\d\\d.\\d\\d");   // matches words beginning by "sub"
    std::string output;

    while (std::regex_search (s,m,e)) {
        for (auto x:m) 
            output = x;
        break;
    }

    return std::stof(output);
}


std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), static_cast<int>(buffer.size()), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}


int main(int argc, char* argv[]) {
    fclose(stderr);
    Corpus corpus; //TODO
    ArgParser argParser(argc, argv);
    Context context(argParser);
    float maxCoverage = 0.0;
    int returnCode = 0;
    int iteration = 0;

    while(returnCode == 0){
        iteration++;
        std::string command, params;
        command += "./program/a.out ";
        for(const auto& param : context.generate())
            params += param;
        command += params;
        int returnCode = system(command.c_str());
        
        std::string coverage = exec("gcov prog.cpp -r -o program");
        float percentage = getPercentage(coverage);
        if(percentage > maxCoverage){
            maxCoverage = percentage;
            std::cout << "next max coverage : " << maxCoverage << std::endl;
            cout << "param: " << params << "; returned: " << returnCode << endl;
            std::cout << "iteration: " << iteration << std::endl; 
        }

        //break;
    
        // checking if the command was executed successfully
        if (returnCode == 0) {
            ;//cout << "Success with param: " << param << endl;
        } else {
            cout << "error" << std::endl;
            //cout << "error with param: " << param << "; returned: " << returnCode << endl;
            //std::cout << "iteration: " << iteration << std::endl; 
        }

        if(iteration % 1'000 == 0){
            cout << "param: ";
            for(const auto& param: params)
                cout << param << " ";
            std::cout << "; returned: " << returnCode << endl;
            std::cout << "iteration: " << iteration << std::endl; 
        }
    }
 
    return 0;
}