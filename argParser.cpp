#include <iostream>
#include <cstring>

#include "argParser.h"

ArgParser::ArgParser(){

}

ArgParser::ArgParser(int argc, char* argv[]){
    _argc = argc;
    _argv = argv;
}

std::vector<ArgType> ArgParser::argsTypes() const{
    std::vector<ArgType> types;
    for(int i = 0; i < _argc; ++i) {
        if (strcmp(_argv[i], "int") == 0)
            types.push_back(ArgType::INT);
        else if(strcmp(_argv[i], "str") == 0)
            types.push_back(ArgType::STR);
    }
    
    return types;
}