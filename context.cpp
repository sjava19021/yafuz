#include <vector>
#include <random>

#include "context.h" 

Context::Context(){
    _argsCount = 0;
}

Context::Context(const ArgParser& argParser){
    setArgParser(argParser);
}

Context::Context(std::vector<ArgType> argsTypes){
    _argsTypes = argsTypes;
    _argsCount = argsTypes.size();
}

Context::Context(int argsCount, std::vector<ArgType> argsTypes){
    _argsCount = argsCount;
    _argsTypes = argsTypes;
}

int Context::randomIntGenerator(int upperBound){
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> distribution(0, upperBound);
    return distribution(rng);
}

std::string Context::randomStrGenerator(){
    std::string generatedString;
    int maxStringLength = 256;
    int randomStringLength = randomIntGenerator(maxStringLength);
    for(int i = 0; i < randomStringLength; ++i){
        generatedString.push_back('a' + randomIntGenerator('z'-'a'));
    }
    return generatedString;
}

void Context::setArgParser(const ArgParser& argParser) {
    _argsTypes = argParser.argsTypes();
    _argsCount = _argsTypes.size();
}

std::vector<std::string> Context::generate(){
    std::vector<std::string> generatedSamples;
    for(auto argType : _argsTypes){
        switch (argType)
        {
        case ArgType::INT: {
            int randomInt = randomIntGenerator();
            generatedSamples.push_back(std::to_string(randomInt));
            break;
        }
        case ArgType::STR:
            generatedSamples.push_back(randomStrGenerator());
            break;

        default:
            break;

        }
    }
    return generatedSamples;
}