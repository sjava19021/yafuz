#include <iostream>

int func(int param) {
    if (param % 5 == 0){
        return 4;
    } else if(param % 1000 == 0){
        return 1;
    } else if (param % 1111 == 0){
        return 2;
    } else if (param % 1777 == 0){
        return 3;
    }
    return 0;
}

int func2(std::string s){
    if(s.size() == 3 && s[0] == 'a'){
        if(s[1] == 'b'){
            if(s[2] == 'c'){
                return 1;
            }
        }
    }
    return 0;
}



int main(int argc, char* argv[]){
    // std::cout << "argc = " << argc << std::endl;
    // std::cout << "argv[1] = " << argv[1] << std::endl;
    // int ret = func(std::atoi(argv[1]));
    //std::cout << "ret = " << ret << std::endl;
    return func(std::atoi(argv[1]));
    //return func2(argv[1]);
}